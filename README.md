# Project description  

Products json file is loaded in the service class. This might be updated to be laoded from a different source.  

To test this service, you should make a `POST` request with the whole order as a json to `index.php` file.  

The service will return the order json updated like this:  

- a new key will be added to the order json called `discounts`. This is an array containg the applied discounts. The order total is not modified and should be modified in front end  
- if discount rule 2 is applied, the number of free products are added to the order item but the total is not changed (because the products are free)  

