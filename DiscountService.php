<?php
/**
 * Discount service class
 */
class DiscountService {
  // posted order json
  private $order;
  
  // products list that will be used to check discounts against
  private $productsList = [];

  // flag that constrolls how discounts are applied:
  // - true will apply only one discount
  // - false will apply all possible discounts
  private $exclusiveDiscounts = false;

  private function __construct($json = "{}") {
    $this->order = json_decode($json);
    $this->order['discounts'] = [];
    $this->getProductsList();
  }

  /**
   * Apply discounts based on the $exclusiveDiscounts flag 
   * 
   * @return $order as json
   */
  public function process(){
    $i = 1;
    if( $this->exclusiveDiscounts ) {
      while( count($this->order['discounts']) == 0 && $i < 4 ) {
        $ruleName = "applyRule" . $i;
        $this->$ruleName();
        $i++;
      }
    } else {
      while( $i < 4 ) {
        $ruleName = "applyRule" . $i;
        $this->$ruleName();
        $i++;
      }
    }
    return json_encode($this->order);
  }

  /**
   * Loads products from the json file
   * but can be plugged in to load products from an api
   */
  private function getProductsList(){
    $productsList = json_decode(file_get_contents('./productsList.json'));
    foreach($productsList as $product) {
      $this->productsList[$product['id']] = $product;
    }
  }

  /**
   *  Rule 1: 10% discount on orders of €1000 or more
   */
  private function applyRule1(){
    if( $this->order['total'] >= 1000 ) {
      $discount = new stdClass();
      $discount->type = "10_percend_discount";
      $discount->description = "10% discount";
      $discount->value = number_format($this->order['total'] / 10, 2);

      $this->order['discounts'][] = $discount;
    }
  }
  /**
   *  Rule 2: get 1 free when buying 5 switches
   */
  private function applyRule2(){
    $categoryId = 2;
    foreach( $this->order['items'] as &$item ) {
      if( $this->productsList[$item['product-id']]['category'] == $categoryId && floor($item['quantity'] / 5) > 0 ) {
        $discount = new stdClass();
        $discount->type = "get_1_free_when_buy_5";
        $discount->description = "1 free '" . $this->productsList[$item['product-id']]['description'] . "'";
        $discount->value = 0;//number_format(floor($item['quantity'] / 5) * $this->productsList[$item['product-id']]['price'], 2);
        $item['quantity'] += floor($item['quantity'] / 5);
        $this->order['discounts'][] = $discount;
      }
    }
  }
  /**
   *  Rule 3: 20% discount on cheapest product when buying 2 Tools
   */
  private function applyRule3(){
    $categoryId = 1;
    $totalProductsInCategory = 0;
    $cheapestPrice = 0;
    $cheapestIndex = 0;
    for( $i = 0; $i < count($this->order['items']); $i++ ) {
      if( $this->productsList[$this->order['items'][$i]['product-id']]['category'] == $categoryId ) {
        $totalProductsInCategory += $this->order['items'][$i]['quantity'];
        if( $cheapestPrice == 0 || ($cheapestPrice > $this->order['items'][$i]['total']) ) {
          $cheapestPrice = $this->order['items'][$i]['total'];
          $cheapestIndex = $i;
        }
      }
    }
    if( $totalProductsInCategory >=2 ) {
      $discount = new stdClass();
      $discount->type = "20_percent_discount_on_product";
      $discount->description = "20% discount on '" . $this->productsList[$this->order['items'][$cheapestIndex]['product-id']]['description'] . "'";
      $discount->value = number_format($cheapestPrice / 5, 2);

      $this->order['discounts'][] = $discount;
    }
  }
}
