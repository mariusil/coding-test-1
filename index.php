<?php

include('DiscountService.php');
// get posted data as a json
$postedData = file_get_contents('php://input');

// make sure the response headers are set
header('Content-Type: application/json');
// check if the script receives posted data and if it is a json
if($postedData) {
  $json = json_decode($postedData, true);
  if( $json['id'] ) {
    $discountService = new DiscountService($json);
    // apply discounts
    $order = $discountService->process();
    http_response_code(200);
    print $order;
    exit;
  }
}
// return default error message and status code
$message = [
  'error' => 'Invalid data received.'
];
http_response_code(400);
print json_encode($message);